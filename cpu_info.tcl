proc get_cpus {} {
    set cpus_file [open "/proc/cpuinfo" r]
    # set cpus [list]
    set cpus [list]
    set cpu [dict create]
    while { [gets $cpus_file line] >= 0 } {
	if [string equal "" $line] {
	    lappend cpus $cpu
	    set cpu [dict create]
	} else {
	    set toks [split $line :]
	    set key [string trim [lindex $toks 0]]
	    set val [string trim [lindex $toks 1]]
	    dict set cpu $key $val
	}
    }   
    close $cpus_file
    return $cpus
}

proc get_freqs {} {
    set cpus [get_cpus]
    set freq_dict [dict create]
    foreach cpu $cpus {
	set core_id [dict get $cpu "core id"]
	set freq [dict get $cpu "cpu MHz"]
	dict set freq_dict $core_id $freq
    }
    set pairs [list]
    foreach core_id [dict keys $freq_dict] {
	lappend pairs [list $core_id [dict get $freq_dict $core_id]]
    }
    set freqs [list]
    foreach pair [lsort -integer -index 0 $pairs] {
	set freq [lindex $pair 1]
	lappend freqs $freq
    }
    return $freqs
}

proc get_cpu_usage {} {
    foreach line [split [exec "top" "-bn1"] "\n"] {
	if [regexp {^%Cpu} $line] {
	    regexp {[0-9\.]+} $line cpu_usage
	    return $cpu_usage
	}
    }
    return -
}

