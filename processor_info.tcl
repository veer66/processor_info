#!/usr/bin/env wish

source [file join [file dirname [info script]] cpu_info.tcl]

proc get_model_name {} {
    set cpus_file [open "/proc/cpuinfo" r]
    while { [gets $cpus_file line] >= 0 } {
	if [regexp {^model name} $line] {
	    return [regsub {^model name[ \t]+:[ \t]+} $line ""]	
	}
    }   
    close $cpus_file
    return "-"
}

proc get_temp {} {
    foreach line [split [exec sensors] "\n"] {
	if [regexp {Tctl} $line] {
	    regexp {[+\-][0-9]+\.[0-9]+°C} $line temp
	    return $temp
	}
    }
    return "-"
}

proc init_model_ui {} {
    label .l_model -text "Model" -width 10
    grid .l_model -column 0 -row 0
    label .v_model -text [get_model_name]
    grid .v_model -column 1 -row 0 -columnspan 3
}

proc init_cpu_temp_ui {} {
    label .l_cpu_temp -text "CPU Temp." -width 10
    grid .l_cpu_temp -column 2 -row 1
    label .v_cpu_temp -text "" -width 10
    grid .v_cpu_temp -column 3 -row 1
}

proc init_freq_ui {} {
    set n [llength [get_freqs]]
    for {set i 0} {$i < $n} {incr i} {
	label ".l_freq_$i" -text "Core $i" -width 10
	grid ".l_freq_$i" -column 0 -row [expr {$i + 1}]
	label ".v_freq_$i" -text "" -width 15
	grid ".v_freq_$i" -column 1 -row [expr {$i + 1}]
    }
}

proc init_cpu_usage_ui {} {
    label .l_cpu_usage -text "CPU Usage" -width 10
    grid .l_cpu_usage -column 2 -row 2
    label .v_cpu_usage -text "" -width 10
    grid .v_cpu_usage -column 3 -row 2
}

proc init_ui {} {
    init_model_ui
    init_freq_ui
    init_cpu_temp_ui
    init_cpu_usage_ui
}

proc update_freqs {} {
    set freqs [get_freqs]
    set n [llength $freqs]
    for {set i 0} {$i < $n} {incr i} {
	set freq [lindex $freqs $i]
	".v_freq_$i" configure -text "$freq MHz" 
    }
}

proc update_temp {} {
    ".v_cpu_temp" configure -text [get_temp]
}

proc update_cpu_usage {} {
    set usage  [get_cpu_usage]
    .v_cpu_usage configure -text "$usage%"
}

proc update_info {} {
    update_freqs
    update_temp
    update_cpu_usage
    after idle [list after 1000 update_info]
}

init_ui
update_info

