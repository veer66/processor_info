# processor_info

Processor_info is a small GUI app showing CPU frequencies, model names, and temperature.

# Prerequisites

* GNU/Linux
* Tcl/Tk
* lm-sensors

# How to run

```
$ chmod 755 processor_info.tcl
$ ./processor_info.tcl
```